import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class RegistrationController {
    async registerUser(newUser: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body(newUser)
            .send();
        return response;
    }
}
