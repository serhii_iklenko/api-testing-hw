import { ApiRequest } from "../request";

export class LoginController {
    async loginUser(email: string, password: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Auth/login`)
            .body({
                email: email,
                password: password,
            })
            .send();
        return response;
    }
}
