import { ApiRequest } from "../request";

const baseUrl:string = global.appConfig.baseUrl;

export class PostsController  {
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

    async createNewPost(postData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/`)
            .body(postData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async addLikeToPost(postData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body(postData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async addCommentToPost(postData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body(postData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}   