import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { expect } from "chai";

const usersController = new UsersController();

describe(`Delete Current User`, () => {
    let userId: number;
    let accessToken: string;

    before(async () => {
        accessToken = global.appConfig.accessToken;
        const currentUserResponse = await usersController.getCurrentUser(accessToken);
        userId = currentUserResponse.body.id;
        // console.log("Access Token:", accessToken);
        // console.log(userId)
    });

    it(`should delete the current user`, async () => {
        expect(userId).to.be.greaterThan(1);
        const response = await usersController.deleteUserById(userId, accessToken);

        checkStatusCode(response, 204);
        checkResponseTime(response, 1000);
        // console.log(response.body)
    });

    afterEach(() => {
        console.log("it was a test");
    });
});
