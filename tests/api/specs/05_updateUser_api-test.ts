import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const schemas = require("./data/schemas_testData.json");

describe("Update user", () => {
    let userDataBeforeUpdate, userDataToUpdate;

    it(`should return correct details of the currect user`, async () => {
        const accessToken = global.appConfig.accessToken;
        let response = await users.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_userDetails);
        userDataBeforeUpdate = response.body;
        // console.log(response.body);
    });

    it(`should update username using valid data`, async () => {
        // replace the last 3 characters of actual username with random characters.
        // Another data should be without changes
        function replaceLastThreeWithRandom(str: string): string {
            return str.slice(0, -3) + Math.random().toString(36).substring(2, 5);
        }

        userDataToUpdate = {
            id: userDataBeforeUpdate.id,
            avatar: userDataBeforeUpdate.avatar,
            email: userDataBeforeUpdate.email,
            userName: replaceLastThreeWithRandom(userDataBeforeUpdate.userName),
        };
        const accessToken = global.appConfig.accessToken;
        let response = await users.updateUser(userDataToUpdate, accessToken);
        checkStatusCode(response, 204);
        // console.log(response.body);
    });

    it(`should return correct user details by id after updating`, async () => {
        let response = await users.getUserById(userDataBeforeUpdate.id);
        checkStatusCode(response, 200);
        expect(response.body).to.be.deep.equal(userDataToUpdate, "User details isn't correct");
        // console.log(response.body);
    });

    afterEach(() => {
        console.log("it was a test");
    });
});
