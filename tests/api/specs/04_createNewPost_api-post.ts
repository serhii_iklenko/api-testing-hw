import { PostsController } from "../lib/controllers/posts.controller";
import { expect } from "chai";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const newPost = new PostsController();
const schemas = require("./data/schemas_testData.json");

let postData;

describe(`Create Post`, () => {
    it(`should create a new post`, async () => {
        postData = {
            previewImage: "cwcwecw",
            body: "Ccascerve",
        };

        const accessToken = global.appConfig.accessToken;
        const response = await newPost.createNewPost(postData, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_postCreate);

        const postID = response.body.id;
        const authorID = response.body.author.id;
        global.appConfig.postID = postID;
        global.appConfig.authorID = authorID;
    });

    it(`should return 401 error when adding post with invalid token`, async () => {
        let invalidToken = "123133";

        const response = await newPost.createNewPost(postData, invalidToken);

        checkStatusCode(response, 401);
        checkResponseTime(response, 1000);
    });

    it(`should return 400 error when a new post with a missing body`, async () => {
        postData = "";

        const accessToken = global.appConfig.accessToken;
        const response = await newPost.createNewPost(postData, accessToken);

        checkStatusCode(response, 400);
        checkResponseTime(response, 1000);
    });
});
