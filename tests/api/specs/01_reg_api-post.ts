import { expect } from "chai";
import { RegistrationController } from "../lib/controllers/reg.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const registrationController = new RegistrationController();
const schemas = require("./data/schemas_testData.json");
const chai = require("chai");
chai.use(require("chai-json-schema"));

describe(`User Registration`, () => {
    it(`should register a new user`, async () => {
        const newUser = {
            id: 0,
            username: global.appConfig.users.registration.username,
            email: global.appConfig.users.registration.email,
            password: global.appConfig.users.registration.password,
            avatar: global.appConfig.users.registration.avatar,
        };

        const response = await registrationController.registerUser(newUser);
        // console.log(response.body);

        checkStatusCode(response, 201);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_registration);
    });
});
