import { PostsController } from "../lib/controllers/posts.controller";
import { expect } from "chai";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const newPost = new PostsController();
const schemas = require("./data/schemas_testData.json");

let postData;

describe(`Add a comment to the post`, () => {
    it(`should add a new comment to the post`, async () => {
        const postID = global.appConfig.postID;
        const authorID = global.appConfig.authorID;

        postData = {
            authorId: authorID,
            postId: postID,
            body: "After the latest update, this game has become better",
        };

        const accessToken = global.appConfig.accessToken;
        const response = await newPost.addCommentToPost(postData, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_postCreate);

        // console.log(response.body);
    });

    it(`should return 400 error when a new post with a missing body`, async () => {
        postData = "";

        const accessToken = global.appConfig.accessToken;
        const response = await newPost.createNewPost(postData, accessToken);

        checkStatusCode(response, 400);
        checkResponseTime(response, 1000);
    });
});
