import { UsersController } from "../lib/controllers/users.controller";
import { expect } from "chai";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const usersController = new UsersController();
const schemas = require("./data/schemas_testData.json");

describe(`Get User Details`, () => {
    it(`should return correct details of the current user`, async () => {
        const accessToken = global.appConfig.accessToken;
        let response = await usersController.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_userDetails);
        // console.log("Access Token:", accessToken);
        // console.log("Data User", response.body);
    });
});
