import { expect } from "chai";
import { LoginController } from "../lib/controllers/login.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const auth = new LoginController();
const schemas = require("./data/schemas_testData.json");

const logEmail = global.appConfig.users.login.email;
const logPass = global.appConfig.users.login.password;

describe("Login User", () => {
    it(`Login and get the token`, async () => {
        const response = await auth.loginUser(logEmail, logPass);
        const accessToken = response.body.token.accessToken.token;
        global.appConfig.accessToken = accessToken; // We save the token in a global object

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_login);
        // console.log("Access Token:", accessToken);
        // console.log("Data User", response.body);
        return accessToken;
    });
});
