import { PostsController } from "../lib/controllers/posts.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { expect } from "chai";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const newPost = new PostsController();
const user = new UsersController();
const schemas = require("./data/schemas_testData.json");

let postData;

describe(`Add a Like to the post`, () => {
    it(`should add a like to the post`, async () => {
        const postID = global.appConfig.postID;
        const authorID = global.appConfig.authorID;

        postData = {
            entityId: postID,
            isLike: true,
            userId: authorID,
        };

        const accessToken = global.appConfig.accessToken;
        const response = await newPost.addLikeToPost(postData, accessToken);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_addLike);

        console.log(response.body);
    });
});
