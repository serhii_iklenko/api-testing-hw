import { UsersController } from "../lib/controllers/users.controller";
import { expect } from "chai";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const usersController = new UsersController();
const schemas = require("./data/schemas_testData.json");

describe(`Get User Details by ID`, () => {
    it(`should get details of a user by their ID`, async () => {
        const accessToken = global.appConfig.accessToken;
        const currentUserResponse = await usersController.getCurrentUser(accessToken);
        const userId = currentUserResponse.body.id;

        const response = await usersController.getUserById(userId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body).to.be.jsonSchema(schemas.schema_userDetails);

        // console.log(response.body);
    });
});
